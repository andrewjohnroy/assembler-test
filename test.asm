default rel

section .rodata
msg: db "One for you, one for me.", 0

section .text
global two_fer
two_fer:
    xor rdx, rdx
    movzx rcx, byte [msg]
    mov [rsi], rcx
    mov byte [rsi+8], 0
    ret